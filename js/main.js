llamadoFetch =() =>{

    const url = "https://jsonplaceholder.typecode.com/todos"
    fetch(url)

    .then(respuesta => respuesta.json())
    .then(data => mostrarTodos(data))
    .catch((reject)=>{

        console.log("Surgio un error" + reject)

    })

}

const llamadoAwait = async () =>{

    try{

        const url = "https://jsonplaceholder.typecode.com/todos"
        const respuesta = await fetch(url)
        const data = await respuesta.json()
        mostrarTodos(data);

    }catch(error){
        console.log("Surgio un error " + error);
    }

}

//Usando Axion
const llamadoAxion = async() =>{
    const url = "https://jsonplaceholder.typecode.com/todos"

    axios
    .get(url)
    .then((res)=>{

        mostrarTodos(res.data)

    })

    .catch((err) =>{

        console.log("Surgio un error " + err)

    })

}

const mostrarTodos =(data) =>{

    console.log(data)

    const res = document.getElementById('respuesta');
    res.innerHTML = "";

    for(let item of data){

        res.innerHTML += item.userId + " , " + item.id + " , " + item.title + " , " + item.complete

    }

}

document.getElementById("btnCargarP").addEventListener('click',function(){

    llamadoFetch();

})