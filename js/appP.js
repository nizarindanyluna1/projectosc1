// USANDO AXIOS
const llamandoAxios = async () => {

    const url = "https://dog.ceo/api/breeds/list";
    axios
        .get(url)
        .then((res) => {
        mostrarDatosRazas(data));
        })
        .catch((err) => {
        console.log("Surgió un error" + err);
        });   
        
  };

  /*
  const url = "https://dog.ceo/api/breeds/list";
    
    fetch(url)
    .then((response) => response.json())
    .then((data) => mostrarDatosRazas(data))
    .catch((error) => {
        const lblError = document.getElementById("lnlError");
        alert("Ocurrió un error: " + error);
    });
    
  
  };
  */ */
  
  const mostrarDatosRazas = (data) => {
    console.log(data);
    console.log(data.message[0]);
    const razas = document.getElementById('razas');
    for(const datos of data.message){ 
        var opcion = document.createElement("option");
        opcion.text = datos;
        razas.add(opcion);
    }
    
  };
  
  const mostrarDatosImagen = (data) => {
    
    console.log(data.message);
    const picture = document.getElementById('imagenRaza');
    picture.innerHTML='';
    const img = document.createElement('img');
    img.src = data.message;
    picture.appendChild(img);
  };


    document.getElementById("btnCargar").addEventListener("click",llamandoFetchRazas);
    document.getElementById("btnImagen").addEventListener("click",llamandoFetchImagen);