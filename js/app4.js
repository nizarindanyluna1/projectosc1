
function cargarDatos(){
    
    const http = new XMLHttpRequest;
    const numero = document.getElementById('numero').value;

    if(numero == ''){
        
        alert("Ingrese un numero");

    } else{

        if(numero >= 1 && numero <= 10){

            const url = "https://jsonplaceholder.typicode.com/users/" + numero;

            //Realizar funcion de respuesta 
            http.onreadystatechange = function(){
                
                //validar respuesta
                if(this.status == 200 && this.readyState==4){

                    let res = document.getElementById('lista');
                    const json = JSON.parse(this.responseText);
                    
                    const datos = json;
                    const address = datos.address;
                    const geo = address.geo;
                    const company = datos.company;

                    res.innerHTML = '<tr> <td class="columna1">' + datos.id + '</td>'
                    + '<td class=columna2">' + datos.name + '</td>'
                    + '<td class=columna3">' + datos.username + '</td>'
                    + '<td class=columna4">' + datos.email + '</td>'
                    + '<td class=columna5"> <ul> <li> ' + address.street + '</li>' 
                    + '<li>' + address.suite + '</li>' +  '<li>' + address.city + '</li>'
                    + '<li>' + address.zipcode + '</li>'
                    + '<li> geo: <ol> <li>' + geo.lat + '</li>' + '<li>' + geo.lng + '</li>'
                    + '<ol> </li>' + '</li> </td>'
                    + '<td class=columna6">' + datos.phone + '</td>'
                    + '<td class=columna7">' + datos.website + '</td>'
                    + '<td class=columna8"> <ul> <li>' + company.name + '</li>'
                    + '<li>' + company.catchPhrase + '</li>' + '<li>' + company.bs + '</li> </tr>';

                } //else alert("Surgio un error al hacer la peticion")

            }

            http.open('GET',url,true);
            http.send();

        }else{

            alert("Numero invalido. Ingrese otro numero");

        }
    }   

}

//Botones
document.getElementById("btnCargar").addEventListener("click",cargarDatos);
document.getElementById("btnLimpiar").addEventListener("click",function(){

    let res = document.getElementById('lista');
    res.innerHTML="";

    let pepe = document.getElementById('numero');   //creando una variable que hace referencia 
    pepe.value="";  //value porque es un input

})