
function cargarDatos(){
    
    const http = new XMLHttpRequest;
    const numero = document.getElementById('numero').value;

    if(numero == ''){
        
        alert("Ingrese un numero");

    } else{

        if(numero >= 1 && numero <= 100){

            const url = "https://jsonplaceholder.typicode.com/albums/" + numero;

            //Realizar funcion de respuesta 
            http.onreadystatechange = function(){
            
                //validar respuesta
                if(this.status == 200 && this.readyState==4){

                    let res = document.getElementById('lista');
                    const json = JSON.parse(this.responseText);

                    //ciclo para mostrar los datos en la tabla
                    const datos = json;

                    res.innerHTML = '<tr> <td class="columna1">' + datos.userId + '</td>'
                    + ' <td class="columna2">' + datos.id + '</td>'
                    + ' <td class="columna3">' + datos.title + '</td> </tr>'

                } //else alert("Surgio un error al hacer la peticion")

            }

            http.open('GET',url,true);
            http.send();

        }else{

            alert("Numero invalido. Ingrese otro numero");

        }

    }

}

//Botones
document.getElementById("btnCargar").addEventListener("click",cargarDatos);
document.getElementById("btnLimpiar").addEventListener("click",function(){

    let res = document.getElementById('lista');
    res.innerHTML="";

    let pepe = document.getElementById('numero');   //creando una variable que hace referencia 
    pepe.value="";  //value porque es un input

})