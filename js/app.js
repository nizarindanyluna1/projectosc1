
function cargarDatos(){
    
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/albums";

    //Realizar funcion de respuesta 
    http.onreadystatechange = function(){
        
        //validar respuesta
        if(this.status == 200 && this.readyState==4){

            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);

            //ciclo para mostrar los datos en la tabla
            for(const datos of json){   //Crea la variable y la va iterando hasta que json se quede vacio

                res.innerHTML += '<tr> <td class="columna1">' + datos.userId + '</td>'
                + ' <td class="columna2">' + datos.id + '</td>'
                + ' <td class="columna3">' + datos.title + '</td> </tr>'

            }

        } //else alert("Surgio un error al hacer la peticion")

    }

    http.open('GET',url,true);
    http.send();

}

//Botones
document.getElementById("btnCargar").addEventListener("click",cargarDatos);
document.getElementById("btnLimpiar").addEventListener("click",function(){

    let res = document.getElementById('lista');
    res.innerHTML="";

})
